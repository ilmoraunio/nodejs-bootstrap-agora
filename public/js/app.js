var Agora = angular.module("Agora", []);

// This directive was supposed to manipulate the #scrollbox to scroll all the way down upon each call of scope.scrollDown()
Agora.directive("scrollBox", function ($window) {
	var linkFn;
	linkFn = function (scope, element, attrs) {
		scrollbox = element.children()[0];

		scope.scrollDown = function () {
			console.log("scrollDown");
			$(scrollbox).scrollTop($(scrollbox)[0].scrollHeight);
		}
	};
	
	return {
		restrict: "E"
		, link: linkFn
	}
});

// Global socket variable
Agora.factory("socket", function ($rootScope) {
	var socket = io.connect();
	return {
		on: function (eventName, callback) {
			socket.on(eventName, function () {
				var args = arguments;
				$rootScope.$apply(function () {
					callback.apply(socket, args);
				});
			});
		},
		emit: function (eventName, data, callback) {
			socket.emit(eventName, data, function () {
				var args = arguments;
				$rootScope.$apply(function () {
					if (callback) {
						callback.apply(socket, args);
					}
				});
			});
		}
	};
});
