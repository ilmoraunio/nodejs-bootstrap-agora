function ChatController ($scope, socket) {
	$scope.inputUsername = "";
	$scope.inputTargetChannel = "";
	$scope.inputAttemptedPassword = "";
	$scope.joinClass = "hidden";
	
	$scope.channels = [
		//  {_id: "514cf75e07f1f8db22000001", name: "Test", class: "active"}
		//, {_id: "514cf79ccd3400ef22000001", name: "Test2", class: ""}
	];
	
	$scope.comments = [
		  {_id: "none", message: "Start posting!", poster: "Agora"}
	];
	
	$scope.active = null;
	
	$scope.select = function (channel) {
		if ($scope.active != channel._id) {
			console.log("Put channel in focus: " + channel._id);
			$scope.channels.forEach(function (elem, index, ar) {
				elem.class = "";
			});
			$scope.active = channel._id;
			socket.emit("listcontents", $scope.active);
			channel.class = "active";
		}
	}
	
	$scope.send = function () {
		if (!$scope.inputMessage || $scope.inputMessage == "") {
			$scope.inputMessage = "";
			console.log("No message sent!");
		} else {
			console.log("Message: " + $scope.inputMessage + " sent!");
			
			// Send message object to express and confirm user credentials
			
			$scope.data = {
				  _channelid: $scope.active
				, _message: $scope.inputMessage
			};
			
			socket.emit("addcomment", $scope.data);
			
			$scope.comments.push({message: $scope.inputMessage, poster: $scope.inputUsername});
			$scope.inputMessage = "";
		}
	}
	
	$scope.springJoin = function () {
		$scope.joinClass = "";
	}
	
	$scope.join = function (input) {
		if ($scope.inputTargetChannel != "") {
			// Send channel and potential password to backend and return with channel data
			
			data = { name: $scope.inputTargetChannel, secret: $scope.inputAttemptedPassword };
			
			socket.emit("joinchannel", data);
		}
	}
	
	$scope.leave = function (channel) {		
		socket.emit("leavechannel", channel._id);
		
		// Find channel's index
		var removableIndex = $scope.channels.indexOf(channel);
		
		$scope.channels.forEach(function (elem, index, ar) {
			elem.class = "";
		});
		
		$scope.active = "";
		
		// If an index is found, remove the element in question
		if (~removableIndex) $scope.channels.splice(removableIndex, 1);
		
		$scope.comments = [];
		console.log("Left channel!");
	}
	
	$scope.springConnect = function () {
		$scope.connectClass = "";
	}
	
	$scope.login = function () {
		if ($scope.inputUsername != "") {
			socket.emit("adduser", $scope.inputUsername);
		}
	}
	
	$scope.connectWithGoogle = function () {
		// Work in progress!
	}
	
	$scope.logout = function () {
		socket.emit("logout");
		console.log("User has logged out!");
		$scope.channels = [];
		$scope.comments = [];
		$scope.connectClass = "";
	}
	
	$scope.list = function () {
		
	}
	
	/* socket.on */
	/* // Scroll to bottom
		$("#scrollbox").scrollTop(
			$("#scrollbox")[0].scrollHeight
		); */
	
	socket.on("commentupdate", function (data) {
		$scope.comments.push(data);
	});
	
	socket.on("listcontents", function (data) {
		$scope.comments = [];
		data.forEach(function (elem, index, ar) {
			$scope.comments.push(elem);
		});
		
	});
	
	socket.on("resultback", function (data) {
		console.log(data);
	});
	
	socket.on("allowjoin", function (channel) {
		$scope.channels.forEach(function (elem, index, ar) {
			elem.class = "";
		});
		
		$scope.active = channel._id;
		
		console.log("Joined a channel!");
		$scope.joinClass = "hidden";
		
		$scope.channels.push({_id: channel._id, name: channel.name, class: channel.class});
		
		// Get new channel's contents
		socket.emit("listcontents", $scope.active);
	});
	
	socket.on("denyjoin", function () {
		console.log("User has been denied access to channel!");
	});
	
	socket.on("allowconnect", function (username) {
		$scope.inputUsername = username;
		console.log("User has logged in!");
		$scope.connectClass = "hidden";
	});
	
	socket.on("denyconnect", function () {
		console.log("User has been denied access!");
	});
	
	socket.on("ping", function (data) {
		console.log(data);
		socket.emit("pong", data);
	});
	
	$scope.$on("$destroy", function (event) {
		socket.emit("logout");
		socket.removeAllListeners();
	});
}
