module.exports = function(app, express, mongoose) {
	var config = this;
	
	app.requireAuth = false;
	
	// configure everyauth
	app.everyauth.twitter
		.consumerKey("yourKey")
		.consumerSecret("yourSecret")
		.findOrCreateUser(function (session, accessToken, accessTokenSecret, user) {
			return 1;
	  }).redirectPath("/");
	  
	// Generic configs
	app.configure(function() {
		app.set("views", __dirname + "/views");
		app.set("view engine", "jade");
		app.set("view options", {
			layout: false
		});
		app.use(express.bodyParser());
		app.use(express.cookieParser());
		app.use(express.session({ 
			  secret: "topSecret"
			, cookie: {httpOnly: true, secure: true}
		}));
		app.use(app.everyauth.middleware());
		app.use(express.methodOverride());
		app.use(app.router);
		app.use(express.static(__dirname + "/public"));
	});
	
	// Environment specific config
	app.configure("development", function () {
		app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
		
		app.mongoose.connect("mongodb://localhost/agora");
		app.mongoose.set("debug", true);
	});
	
	app.configure("production", function () {
		app.use(express.errorHandler());
		app.mongoose.connect("mongodb://[insert remote address here]");
	});
	
	return config;
};
