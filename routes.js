module.exports = function (app, models) {
	
	app.get("/", function (request, response) {
		response.render("index", {
			title: "Agora"
		});
	});

	app.get("/static", function (request, response) {
		response.sendfile("public/mockup.html");
	});
};
