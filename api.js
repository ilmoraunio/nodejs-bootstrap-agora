module.exports = function (models) {
	var User = models.user
	,   Channel = models.channel
	,   Comment = models.comment;
	
	return {
		getUserIfExists: function (username, func) {
			User.count({name: username}, function (err, count) {
				// User doesn't exist
				if (count == 0) {
					console.log("count 0 username: " + username);
					func(username);
				// User exists
				} else {
					User.findOne({name: username}, function (err, found) {
						func(found.name, found._id);
					});
				}
			});
		},
		
		addUser: function (username, func) {
			new User({"name": username}).save(function (err) {
				if (err) return handleError(err);
				User.findOne({"name": username}, function (err, found) {
					func(found.name, found._id);
				});
			});
		},
		
		dropUsers: function () {
			User.remove({}, function (err) {
			});
		},
		
		listUsers: function (_id, func) {
			var collection;
			if (!_id) {
				User.find({}, function (err, users) {
					func(users);
				});
			} else {
				Channel.findOne(
					{"_id": _id}, 
					{_id: false, "users": true})
					.populate("users")
					.exec(function (err, list) {
					
					var users = new Array();
					for (i = 0; i < list.users.length; i++) {
						users.push({_id: list.users[i]._id, name: list.users[i].name});
					}
					
					/* in the end we only have an array of objects with {id, name} per array element */
					func(users);
				});
			}
		},
		
		checkUserBelongs: function (_userid, _channelid, func) {
			Channel.count({_id: _channelid, users: _userid}, function (err, count) {
				if (count == 1) {
					func(true);
				} else {
					func(false);
				}
			});
		},
		
		addChannel: function (channelname, channelsecret, func) {
			var channel;
			if (!channelsecret) {
				new Channel({name: channelname, secret: ""}).save(function (err) {
					Channel.findOne({name: channelname}, function (err, found) {
						func(found._id, found.name);
					});
				});
			} else {
				new Channel({name: channelname, secret: channelsecret}).save(function (err) {
					Channel.findOne({name: channelname}, function (err, found) {
						func(found._id, found.name);
					});
				});
			}
		},
		
		listChannel: function (func) {
			Channel.find({}, function (err, Channel) {
				return func(Channel);
			});
		},
		
		renderChannel: function () {
			Channel.find({}, function (err, Channel) {
				response.render("channel", {
					channels: Channel
				});
			});
		},
		
		dropChannel: function (_channelid) {
			Channel.remove({_id: _channelid}, function (err) {
				if (err) return handleError(err);
			});
		},
		
		checkChannelAccess: function (socket, channelname, channelsecret, func) {
			// if a new channel is created
			// func(var exists = false, socket = null, found.name = null, found._id = null);

			// use case if username + password is not matched
			// func(var exists = true, socket, found.name, null);
			
			// use case if username + password is matched
			// func(var exists = true, socket, found.name, found._id);
			
			Channel.count({name: channelname}, function (err, found) {
				if (found == 0) {
					// Create new
					console.log("Create new");
					func(false, socket, null, null);
				} else {
					// Try to join existing
					console.log("Trying to join an existing one");
					Channel.findOne({name: channelname}, function (err, found) {
						if (found.secret = "" || typeof found.secret == "undefined") {
							console.log("No secret defined");
							func(true, socket, found.name, found._id);
						} else {
							Channel.findOne({name: channelname, secret: channelsecret}, function (err, found) {
								console.log("Secret defined");
								if (!found) {
									console.log("No channel match");
									// A match is not found
									func(true, socket, channelname, null);
								} else {
									console.log("Channel match");
									// A succesful match is found
									func(true, socket, found.name, found._id);
								}
							});
						}
					});
				}
			});
		},
		
		addUserTo: function (_channelid, _userid, func) {
			Channel.update(
			// Find the correct channel
			  {"_id": _channelid}
			// Don't add a duplicate user ref 
			, {$addToSet: {users: _userid}}
			, function (err) {
				Channel.findOne({"_id": _channelid}, function (err, found) {
					func(found._id, found.name);
				});
			});
		},
		
		dropUserFrom: function (_channelid, _userid) {
			Channel.findOne({_id: _channelid}, function (err, channel) {				
				// Find user's index
				var removableIndex = channel.users.indexOf(_userid);
				// If an index is found, remove the element in question
				if (~removableIndex) channel.users.splice(removableIndex, 1);
				
				channel.save();
			});
		},
		
		// Does not work atm, found has no save method and needs to be binded to a mongoose model somehow
		dropUserFromAll: function (socket) {
			Channel.update({users: socket._userid}, {$pull: {users: socket._userid}}, {multi: true}, function (err, numberAffected, doc) {
				// If callback is needed, expand function parameters to include func and invoke func() with necessary arguments 
			});
		},
		
		addCommentTo: function (_channelid, _userid, _message, func) {
			User.findById(_userid, function (err, found) {
				console.log(found);
				console.log(err);
				if (!err) {
					var newComment = new Comment({message: _message, date: new Date(), poster: found.name});
					newComment.save();
					
					Channel.update(
					// Find the correct channel
					  {"_id": _channelid}
					// Don't add a duplicate user ref 
					, {$addToSet: {comments: newComment._id}}
					, function (err) { 
						func(true);
					});
				} else {
					func(false);
				}
			});
		},
		
		listComment: function (_channelid, func) {
			function callback (err, list) {
				// if (err) return handleError(err);
				
				var comments = new Array();
				// Look for one channel's comments
				if (list.length == undefined) {
					for (var i = 0; i < list.comments.length; i++) {
						comments.push(
							{ _id: list.comments[i]._id
							, message: list.comments[i].message
							, date: list.comments[i].date
							, poster: list.comments[i].poster}
						);
					}
				// Look for every comment from each channel
				} else {
					for (var j = 0; j < list.length; j++) {
						for (var i = 0; i < list[j].comments.length; i++) {
							comments.push(
								{ _id: list[j].comments[i]._id
								, message: list[j].comments[i].message
								, date: list[j].comments[i].date
								, poster: list[j].comments[i].poster}
							);
						}
					}
				}
				
				function byDate (a, b) {
					var va = (a.date === null) ? "" : "" + a.date
					,   vb = (b.date === null) ? "" : "" + b.date;
					
					return va > vb ? 1 : (va === vb ? 0 : -1);
				}
				
				comments.sort(byDate);
				
				/* in the end we only have an array of objects with {id, name} per array element */
				return func(comments);
			}
			
			if (!_channelid) {
				Channel.find(
					{}, 
					{_id: false, comments: true})
				.populate("comments")
				.exec(callback);
			} else {
				Channel.findOne(
					{"_id": _channelid}, 
					{_id: false, comments: true})
				.populate("comments")
				.exec(callback);
			}
		}
	};
};
