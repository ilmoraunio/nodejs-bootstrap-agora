module.exports = function (mongoose) {
	var collection	= "User";
	
	var userSchema = require("./userschema")(mongoose);
	this.model = mongoose.model(collection, userSchema, collection);
	
	return this;
}

