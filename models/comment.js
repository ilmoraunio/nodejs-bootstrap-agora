module.exports = function (mongoose) {
	var collection	= "Comment";
	
	var commentSchema = require("./commentschema")(mongoose);
	this.model = mongoose.model(collection, commentSchema, collection);
	
	return this;
}
