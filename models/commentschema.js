module.exports = function (mongoose) {
	var Schema		= mongoose.Schema
	  , ObjectId	= Schema.ObjectId;
	
	var schema = new Schema({
	    message: String
	  , date: Date
	  , poster: String
	});
	
	return schema;
}
