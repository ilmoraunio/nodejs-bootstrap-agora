module.exports = function (mongoose) {
	var collection 	= "Channel";
	
	var channelSchema = require("./channelschema")(mongoose);
	this.model = mongoose.model(collection, channelSchema, collection);
	
	return this;
};
