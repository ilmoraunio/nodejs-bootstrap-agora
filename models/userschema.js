module.exports = function (mongoose) {
	// Run this command in mongodb:
	// db.User.ensureIndex({name:1}, {unique:true, dropDups:true})
	var Schema		= mongoose.Schema
	  , ObjectId	= Schema.ObjectId;
	
	var schema = new Schema({
		name: String
	  , OAuth: String
	});
	
	return schema;
}
