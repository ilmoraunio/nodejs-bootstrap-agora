module.exports = function (mongoose) {	
	// Run this command in mongodb:
	// db.Channel.ensureIndex({name:1}, {unique:true, dropDups:true})
	var Schema		= mongoose.Schema
	  , ObjectId	= Schema.ObjectId;
	 
	var UserSchema		= require("./userschema")(mongoose);
	var CommentSchema	= require("./commentschema")(mongoose);
	
	var schema = new Schema({
		name: {type: String, index: true, unique: true, dropDups:true}
	  , users: [{type: ObjectId, ref: "User"}]
	  , comments: [{type: ObjectId, ref: "Comment"}]
	  , secret: String
	  , private: Boolean
	});
	
	return schema;
}
