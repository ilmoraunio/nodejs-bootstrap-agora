var express = require("express")
  , app 	= module.exports = express()
  , server	= require("http").createServer(app)
  , io		= require("socket.io").listen(server)
  , stylus	= require("stylus")
  , nib		= require("nib")
  , openid	= require("openid");
  
app.everyauth = require("everyauth");
app.mongoose = require("mongoose");

var config = require("./config.js")(app, express);

var models = {};
models.user		= require("./models/user")(app.mongoose).model;
models.channel 	= require("./models/channel")(app.mongoose).model;
models.comment 	= require("./models/comment")(app.mongoose).model;

require("./routes")(app, models);

var api = require("./api")(models);

server.listen(process.env.PORT || 8080);

// Global collection to prevent duplicate users
var usernames = [];

io.sockets.on("connection", function (socket) {
	socket.on("adduser", function (username) {
		
		if (typeof usernames[username] == "object") {
			socket.emit("denyconnect")
			return;
		}
		
		console.log(usernames);
		usernames[username] = {"username": username, "socket": socket};
		
		api.getUserIfExists(username, function (username, _id) {
			if (typeof _id == "undefined") {
				api.addUser(username, function (username, _id) {
					socket._userid = _id;
					socket.username = username;
					socket.emit("allowconnect", username);
				});
			} else {
				socket._userid = _id;
				socket.username = username;
				socket.emit("allowconnect", username);
			}
		});
	});
	
	socket.on("joinchannel", function (data) {
		api.checkChannelAccess(socket, data.name, data.secret, function (exists, socket, channelname, _channelid) {
			if (!exists) {
				// create new channel
				api.addChannel(data.name, data.secret, function (_channelid, channelname) {
					api.addUserTo(_channelid, socket._userid, function (_channelid, channelname) {
						data = {_id: _channelid, name: channelname, class: "active"};
						socket.emit("allowjoin", data);
					});
				});
			} else {
				if (_channelid) {
					console.log(_channelid);
					// Join an existing one
					api.addUserTo(_channelid, socket._userid, function (_channelid, channelname) {
						data = {_id: _channelid, name: channelname, class: "active"};
						socket.emit("allowjoin", data);
					});
				} else {
					// No match is found
					socket.emit("denyjoin");
				}
			}
			
		});
	});
	
	socket.on("leavechannel", function (_channelid) {
		api.dropUserFrom(_channelid, socket._userid);
	});
	
	socket.on("logout", function () {
		delete usernames[socket.username];
		api.dropUserFromAll(socket);
	});
	
	socket.on("addcomment", function (data) {
		api.checkUserBelongs(socket._userid, data._channelid, function (belongsToChannel) {
			if (belongsToChannel) {
				api.listUsers(data._channelid, function (found) {
					found.forEach(function (elem, index, ar) {
						// Broadcast message to other users iff a user is online and there is a match between the channel and a user in the collection usernames
						if (usernames[elem.name] && elem._id.toString() != socket._userid.toString()) {
							console.log("usernames[elem.name].socket is being called.");
							usernames[elem.name].socket.emit("commentupdate", {message: data._message, poster: socket.username});
						}
					});
				});
				
				api.addCommentTo(data._channelid, socket._userid, data._message, function (posted) {
					socket.emit("resultback", {status: posted});
				});
			} else {
				socket.emit("denyjoin");
			}
		});
	});
	
	socket.on("listcontents", function (_channelid) {
		api.listComment(_channelid, function (data) {
			socket.emit("listcontents", data);
		});
	});
	
	socket.on("disconnect", function () {
		delete usernames[socket.username];
		api.dropUserFromAll(socket);
	});
});
