# Agora

Primarily a node.js-driven Bootstrapped chat web app that aims to bring users of different social platforms together under a central chatting environment. The aim is that users would be able to create private and public channels for others to join.

The aims **for this project** is to

* Have fun (no, this is not a serious project).
* Learn how to build interesting stuff with node.js and friends (Express + Socket.io).
* Learn how to use login APIs.
* Practise frontend building.
